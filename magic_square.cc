#include <stdio.h>
#include <iostream>
#include <stdlib.h>

using namespace std;


int main() {
	int** arr;
	int n;
	int x, y;
	cin >> n;

if(n%2==1){

	arr = (int**)malloc(sizeof(int*) * n);

	for (int i = 0; i < n ; i++) {
		arr[i] = (int*)malloc(sizeof(int) * n );
	}
	
		
	x = 0;
	y = n / 2;
	arr[x][y] = 1;

	for (int i = 2; i <= n*n; i++) {
		if (x == 0 && y != n - 1) {
			if (arr[n-1][ y+1] != 0) {
				arr[x + 1][y] = i;
				x=x+1;
			}
			else {
				arr[n-1][y+1] = i;
				x = n - 1;
				y=y+1;
			}
		}

		else if (x == 0 && y == n - 1) {
			arr[x + 1][y] = i;
			x=x+1;
		}

		else if (x != 0 && y == n - 1) {
			if (arr[x - 1][0] != 0) {
				arr[x+1][y] = i;
				x=x+1;
			}
			else {
				arr[x - 1][0] = i;
				x--;
				y = 0;
			}

		}
		else {
			if (arr[x - 1][y + 1] != 0) {
				arr[x + 1][y] = i;
				x=x+1;
			}
			else {
				arr[x-1][y+1] = i;
				x--;
				y++;
			
			}
		}
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			cout << arr[i][j] << " ";
		cout << "\n";
	}

	free(arr);
}
	return 0;
}
